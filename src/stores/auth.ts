import { useMessageStore } from "./message";
import { ref, computed } from "vue";
import { defineStore } from "pinia";
import auth from "@/services/auth";
import { useLoadingStore } from "./loading";
import router from "@/router";

export const useAuthStore = defineStore("auth", () => {
  const messageStore = useMessageStore();
  const loadingStore = useLoadingStore();
  // const authName = ref("");
  // const isAuth = computed(() => {
  //   const user = localStorage.getItem("user");
  //   if (user) {
  //     return true;
  //   }
  //   return false;
  //   // authName is not empty
  //   // return authName.value !== "";
  // });

  const isLogin = () => {
    const user = localStorage.getItem("user");
    if (user) {
      return true;
    }
    return false;
  };

  const login = async (username: string, password: string): Promise<void> => {
    loadingStore.isLoading = true;
    try {
      console.log({ username, password });
      const res = await auth.login(username, password);
      console.log(res);
      localStorage.setItem("user", JSON.stringify(res.data.user));
      localStorage.setItem("token", res.data.access_token);
      console.log("success");
      router.push("/");
    } catch (err) {
      console.log(err);
      messageStore.showError("email หรือ password ไม่ถูกต้อง");
    }
    loadingStore.isLoading = false;
    // localStorage.setItem("loginName", username);
  };
  const logout = () => {
    // authName.value = "";
    // localStorage.removeItem("loginName");
    localStorage.removeItem("user");
    localStorage.removeItem("token");
    router.replace("/login");
  };
  // const loadData = () => {
  //   authName.value = localStorage.getItem("loginName") || "";
  // };

  return { login, logout, isLogin };
});
